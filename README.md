# Nlopt method for extracting summary statistics from Kaplan-Meier survival curves

### Introduction
This is the gitlab page for the method described in a BMC Medical Research Methodology paper: https://bmcmedresmethodol.biomedcentral.com/articles/10.1186/s12874-020-01092-x

This method uses the published P value to better estimate the underlying censoring pattern in a Kaplan-Meier plot.  This is allows a more accurate estimation of the summary statistics to be calculated, especially in the case where the number at risk below the Kaplan-Meier plot is not published. 

### Use
As of 01/11/2020, the best way to use this method is to download the R script (nlopt_km_hr_v1.0.R) from the main page of this repository and run the file locally.  We have also written a Shiny app to create a web inteface to execute the code but this is currently undergoing maintenance.  We expect this to be working within the next few days.

In the meantime, if you have any problems with the code please email Andrew Irvine (a.f.irvine at symbol leeds.ac.uk).
  
Thank you.

Andrew Irvine and Ed Green
## ---------------------------
##
## Script name: app.R
##
## Purpose of script: Shiny frontend for Irvine method of extracting values from KM plots
##
## Author: Dr. Edward Green
##
## Date Created: 2020-03-29
##
## Copyright (c) Edward Green, 2020
## Email: e.green@dkfz.de
##
## ---------------------------
##
## Notes: 
##
## Also available on shinyapps.io
## https://edgreen21.shinyapps.io/km_hr/
##
## ---------------------------


#DEV NOTE: use more shiny:validate functions - check with Andrew on latest progress
#DEV NOTE: may need to have switch for nloptr warning:
    # if ( getOption('nloptr.show.inequality.warning') ) {
    # message('For consistency with the rest of the package the inequality sign may be switched from >= to <= in a future nloptr version.')
    # }

#load libraries (assuming already installed)
if (!require(shiny, quietly = TRUE))             install.packages("shiny")
if (!require(dplyr, quietly = TRUE))             install.packages("dplyr")
if (!require(nloptr, quietly = TRUE))            install.packages("nloptr")
if (!require(readxl, quietly = TRUE))            install.packages("readxl")
if (!require(rhandsontable, quietly = TRUE))     install.packages("rhandsontable")
if (!require(shinycustomloader, quietly = TRUE)) install.packages("shinycustomloader")
if (!require(shinyWidgets, quietly = TRUE))      install.packages("shinyWidgets")
library("tools") #core package


#to deploy to shinyapps.io run the lines below:

# if (!require(rsconnect, quietly = TRUE))             install.packages("rsconnect")
# rsconnect::deployApp(appDir   = "R/shiny",
#                      upload = TRUE,
#                      appFiles = c("R/200928_updated_nlopt_code.R","R/shiny"))

#source the functions written for HR extraction from KM
# source('../IrvineKM.R') 
source('201011_updated_nlopt_code.R') 

ui <- fluidPage(

    # University logo
    tags$img(src='southampton.png', height=150, width=300),
    
    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        
        #sidebar panel for inputs
        sidebarPanel(
            
            #Label box
            tags$b("Input data"),
            
            
            # Horizontal line ----
            tags$hr(),
            
            #file input
            fileInput(inputId     = "file1", 
                      '3 column input file: time, arm1, arm2. *.xlsx, *.xls, or *.csv formats accepted',
                      accept      = c(".xlsx", ".xls", ".csv")),

            # Input: Numeric entry for number Subjects in arm C
            numericInput(inputId  = "risk_t0_arm1",
                         label    = "Subjects in arm 1 (control):",
                         value    = 1,
                         min      = 1,
                         step     = 1),
            
            # Input: Numeric entry for number Subjects in arm D
            numericInput(inputId  = "risk_t0_arm2",
                         label    = "Subjects in arm 2 (experiment):",
                         value    = 1,
                         min      = 1,
                         step     = 1),


            #input value
            radioButtons(inputId = "optimise_type",
                         label = "Optimise for a:",
                         choices =  c("P" = "P_value", "chi-square" = "chi_square"),
                         inline = TRUE, width = NULL, choiceNames = NULL,
                         choiceValues = NULL),
            
            # Input: Chi squre to optimise for 
            numericInput(inputId  = "value_to_optimise",
                         label    = "value of:",
                         value    = 0.000,
                         step     = 0.001),

            
            #button for starting the analysis
            actionButton("submit", "Run nlopt")
            
        ),
        
        
        mainPanel(
          
          tags$b("Kaplan-Meier plot"), 
          
          withLoader(plotOutput("plot"), 
                     type   = "html", 
                     loader = "loader1"),

          tags$b("nloptr analysis"),
          tags$br(),
          
          uiOutput("moreControls"),
        
          # withLoader(verbatimTextOutput("summary"),
          #            type         = "html",
          #            loader       = "loader1"),
         
        
          tags$br(),
          
          withLoader(tableOutput("table"),
                     type         = "html",
                     loader       = "loader1")
          
          
            
              #          
              #          downloadButton(outputId = "download_table", label = "Download Results Table"))
            )
        )
    )


server <- function(input, output, session) {

    
  #Observe user pressing 'Run nlopt' button and return modal dialog if no data is present
  observeEvent(input$submit, {

    if (is.null(input$file1)) {
    showModal(
      modalDialog(title = 'No data uploaded',
                  'Please upload a file before clicking \'Run nlopt\'')
    )}
    
  })
  

 
  
  #reactive to calculate values
  output_table <- eventReactive(input$submit, {
    
    #wrap in a trycatch and add a popup in case no data was used  https://shiny.rstudio.com/articles/action-buttons.html
    req(input$file1, input$value_to_optimise, input$risk_t0_arm1, input$risk_t0_arm2)
    

    #load data
    if (file_ext(input$file1$datapath) == "csv") {
      input_df <- read.csv(input$file1$datapath)
    } else {
      input_df <- read_excel(input$file1$datapath) 
    }
    
    
    #set chi value
    if(input$optimise_type == "P_value"){
      chi_sq_value_to_optimise <<- qchisq(p = input$value_to_optimise, df = 1, lower.tail=FALSE)
    } else{
      chi_sq_value_to_optimise <<- input$value_to_optimise
    }

    #calculate output table
    solve_KM_nlopt(time_vector              = pull(input_df[1]),
                   risk_t0_arm1             = input$risk_t0_arm1, 
                   prob_arm1                = pull(input_df[2]), 
                   risk_t0_arm2             = input$risk_t0_arm2, 
                   prob_arm2                = pull(input_df[3]), 
                   chi_sq_value_to_optimise = input$value_to_optimise) 
  })
  
  
  #plot for uploaded file
  output$plot <- renderPlot({
    
    #validate input 
    validate(need(input$file1,                       "Upload a data set to see KM plot of the input")
             )
    
    #read data
    if (file_ext(input$file1$datapath) == "csv") {
      input_df <- read.csv(input$file1$datapath)
    } else {
      input_df <- read_excel(input$file1$datapath)
    }
    
    #make plot using base graphics - move to ggplot for nicer
    plot(data.frame(input_df[1], input_df[2]),
         type  ="S",
         main  = paste("Kaplan-Meier plot of '", tools::file_path_sans_ext(input$file1$name), "'", sep = ""),
         xlab  = "Time",
         ylab  = "Percent surviving",
         ylim  = c(min(input_df[2],input_df[3]),100))
    
    lines(data.frame(input_df[1], input_df[3]),
          type = "S",
          col  = "red")
    
    legend("topright", legend=c("Arm 1", "Arm  2"), col = c(1, 2), lty=1, cex=1)
  })
  
  
  #render the output data.frame as Shiny output
  output$table <- renderTable({
    
    
    
    
    #validate input 
    validate(need(input$risk_t0_arm1 != 1,           "Set the number of subjects in arm1 (control)"),
             need(input$risk_t0_arm2 != 1,           "Set the number of subjects in arm2 (experiment)"),
             need(input$value_to_optimise != 0.000, 'Set a P or chi square value')
    )
    
    
    req(input$file1)
    
    #load data
    if (file_ext(input$file1$datapath) == "csv") {
      input_df <- read.csv(input$file1$datapath)
    } else {
      input_df <- read_excel(input$file1$datapath) 
    }
    
    output_censor_df <-  tibble(input_df[1],
                                "Arm 1 consor" = output_table()[["Censor_Values"]][c(TRUE, FALSE)],
                                "Arm 2 censor" = output_table()[["Censor_Values"]][c(FALSE, TRUE)])
    
    output_censor_df
    
    })
     
  #Generate a summary of the data ----
  # output$summary <- renderText({
  #   output_table()[["Stats"]]
  #   })
   
  
  output$moreControls <- renderUI({
    
    tagList(
      tags$br(),
      tags$b("RESULTS"),
      tags$br(),
      tags$b("nloptr statistics"),                                       tags$br(),
      ("ChiSq Arm 1          = "), (output_table()[["Stats"]][["Chi_Sq_Arm1"]]),  tags$br(),
      ("ChiSq Arm 2          = "), (output_table()[["Stats"]][["Chi_Sq_Arm2"]]),  tags$br(),
      ("ChiSq combination    = "), (output_table()[["Stats"]][["Chi_Sq_Combo"]]), tags$br(),
      ("P value              = "), (output_table()[["Stats"]][["P-value"]]),      tags$br(),
      tags$br(),
      tags$b("nloptr hazard ratios"),                                                    tags$br(),
      ("HR Arm 1 vs Arm 2    = "), (output_table()[["HR"]][["HR_Arm1_vs_Arm2"]]), tags$br(),
      ("HR CI upper          = "), (output_table()[["HR"]][["HR_CI_upper"]]),     tags$br(),
      ("HR CI lower          = "), (output_table()[["HR"]][["HR_CI_lower"]]),     tags$br(),
      tags$br(),
      tags$b("nloptr invhazard ratios"),                                                        tags$br(),
      ("invHR Arm 1 vs Arm 2 = "), (output_table()[["invHR"]][["invHR_Arm2_vs_Arm1"]]),  tags$br(),
      ("invHR CI upper       = "), (output_table()[["invHR"]][["invHR_CI_upper"]]),      tags$br(),
      ("invHR CI lower       = "), (output_table()[["invHR"]][["invHR_CI_lower"]]),      tags$br(),
      tags$br(),
      tags$b("nloptr estimated censor values")
    )
    
  })
    
  # 
  # #download table button output
  # output$download_table <- downloadHandler(
  #   
  #     filename = function() {
  #         paste("Irvine_KM_analysis_of_", tools::file_path_sans_ext(input$file1$name), "_on_", format(Sys.Date(), "%d-%m-20%y"), "_results_table.csv", sep="")},
  #     content = function(file) {
  #         write.csv(x = output_table()[["Censor_Values"]],
  #                   file,
  #                   row.names = FALSE)}
  # )
  # 
  # 
  # #download table button output
  # output$download_summary <- downloadHandler(
  # 
  #     filename = function() {
  #         paste("Irvine_KM_analysis_of_", tools::file_path_sans_ext(input$file1$name), "_on_", format(Sys.Date(), "%d-%m-20%y"), "_stats_summary.txt", sep="")},
  #     content = function(file) {
  #         capture.output(output_table()[["Stats"]], file =  file)}
  # )
  # 
  # 
  # 
  # #download example data button output
  # output$download_example_1 <- downloadHandler(
  #   
  #   filename = function() {
  #     paste("191127_shiny_input_hanley1b", ".csv", sep="")
  #   },
  #   content = function(file) {
  #     data <- read_excel(path = 'data/Irvine_method_examples/191127_shiny_input_hanley1b.xlsx')
  #     write.table(x=data, file=file, row.names = FALSE, sep = ",") #  error: not able to remove row names using write.xlsx
  #   }
  # )
  # 
  # 
  # 
  # #download example data button output
  # output$download_example_2 <- downloadHandler(
  #   
  #   filename = function() {
  #     paste("191202_little_shiny_input", ".csv", sep="")
  #   },
  #   content = function(file) {
  #     data  <- read_excel(path = 'data/Irvine_method_examples/191202_little_shiny_input.xlsx')
  #     write.table(x=data, file=file, row.names = FALSE, sep = ",") #  error: not able to remove row names using write.xlsx
  #   }
  # )
    
}


# Run the application 
shinyApp(ui = ui, server = server)